import 'package:firedart/firedart.dart';
import 'package:flutter/material.dart';

import 'data/global.dart';
import 'views/index-view.dart';

void main() async {
  //Init Firedart library
  Firestore.initialize(FireBaseProjectId);
  runApp(ChatApp());
}

class ChatApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Chat App',
      theme: ThemeData(
        primaryColor: PrimaryColor,
      ),
      home: IndexView(),
    );
  }
}
