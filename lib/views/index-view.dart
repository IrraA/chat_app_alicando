import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../data/global.dart';
import '../services/api/firebase-api-handler.dart';
import 'components/rounded-button.dart';
import 'signup-login-view.dart';

class IndexView extends StatefulWidget {
  //init imported classes
  final FirebaseAPIHandler firebaseAPIHandler = new FirebaseAPIHandler();
  @override
  State<StatefulWidget> createState() {
    return IndexViewState();
  }
}

class IndexViewState extends State<IndexView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: AppBackgroundColor,
      body: SizedBox(
        width: double.maxFinite,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _appIconImage(deviceSize),
            _signUpButton(),
            _loginButton()
          ],
        ),
      ),
    );
  }

  _appIconImage(Size deviceSize) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 35.0),
      child: SizedBox(
        width: deviceSize.width * 0.5,
        child: Image.asset("assets/images/app_icon.png"),
      ),
    );
  }

  _signUpButton() {
    return RoundedButton(
      onPressed: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => SignUpLoginView(
                      isLogin: false,
                    )));
      },
      color: SecondaryColor,
      title: SignUp,
    );
  }

  _loginButton() {
    return RoundedButton(
      onPressed: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => SignUpLoginView(
                      isLogin: true,
                    )));
      },
      color: PrimaryColor,
      title: Login,
    );
  }
}
