import 'dart:async';
import 'dart:collection';
import 'dart:convert';

import 'package:firedart/firedart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../data/global.dart';
import '../data/models/api-result.dart';
import '../services/api/firebase-api-handler.dart';
import 'components/chat-bubble.dart';
import 'components/common-widgets.dart';

//This is the Chat page of the app
//Can be only accessed after successful sign up or login
//Only one room for all the users

class ChatView extends StatefulWidget {
  final String email;
  final DateTime dateTimeChatStarted;
  ChatView(this.email, this.dateTimeChatStarted);

  //init imported classes
  final FirebaseAPIHandler firebaseAPIHandler = new FirebaseAPIHandler();
  final CommonWidgetsCollection commonWidgets = new CommonWidgetsCollection();
  @override
  State<StatefulWidget> createState() {
    return ChatViewState();
  }
}

class ChatViewState extends State<ChatView> {
  TextEditingController messageController = TextEditingController();
  ScrollController scrollController = ScrollController();
  StreamSubscription _chatSubscription;
  bool isSubscriptionActive = true;
  double _inputHeight = 60;

  @override
  void initState() {
    //initialize subscription for the chat stream
    _initChatSubscription();
    messageController.addListener(_checkInputHeight);
    super.initState();
  }

  @override
  void dispose() {
    //disposes subscription to avoid leaks
    _chatSubscription.cancel();
    messageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        backgroundColor: AppBackgroundColor,
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              widget.commonWidgets.header(true, context),
              _chatBody(),
              _bottomBar()
            ],
          ),
        ),
      ),
    );
  }

  //Method to scroll the lower limit of the scrollview used for the chat body
  //Set to 0.0 since scrollview reverse = true, this is to accurately scroll the scrollview to the bottom
  _scrollToBottom() {
    scrollController.animateTo(
      0.0,
      curve: Curves.easeOut,
      duration: const Duration(milliseconds: 300),
    );
  }

  //The chat body where the collection for chatroom is streamed throughout all users
  _chatBody() {
    return Expanded(
      child: StreamBuilder<List<Document>>(
        stream:
            widget.firebaseAPIHandler.getCollectionStream().get().asStream(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(PrimaryColor)),
            );
          }
          //Processes snapshot data from stream
          List<Document> map = snapshot.data;
          //Only way to sort messages from raw snapshot data because the only other and effective way is using Firestore Cloud plugin

          map.sort((a, b) {
            return DateTime.parse(a.map['createDate'].toString())
                .compareTo(DateTime.parse(b.map['createDate'].toString()));
          });
          List<Widget> messages = map
              .map((doc) => ChatBubble(
                  text: doc['message'],
                  from:
                      doc['username'] == widget.email ? "You" : doc['username'],
                  me: widget.email == doc['username']))
              .toList();

          return Container(
            child: Align(
                alignment: FractionalOffset.bottomCenter,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  padding: const EdgeInsets.symmetric(horizontal: 18),
                  child: SingleChildScrollView(
                    reverse: true,
                    controller: scrollController,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 18),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: messages),
                    ),
                  ),
                )),
          );
        },
      ),
    );
  }

  //Where Textbox and send button are created
  _bottomBar() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 18),
      padding: EdgeInsets.symmetric(vertical: _inputHeight / 10),
      decoration: BoxDecoration(
          border: Border(
              top: BorderSide(color: TextColor.withOpacity(0.3), width: 1.5))),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 13),
        child: Row(
          children: <Widget>[
            Expanded(
              child: SizedBox(
                height: _inputHeight * 0.7,
                child: TextField(
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  onSubmitted: (value) => initSendMessage(),
                  decoration: InputDecoration(
                    hintText: "Start a new message",
                    hintStyle:
                        TextStyle(fontSize: RegularFontSize, color: TextColor),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                      borderSide: BorderSide(width: 0, style: BorderStyle.none),
                    ),
                    filled: true,
                    contentPadding: const EdgeInsets.only(
                        top: 0, bottom: 0, left: 10, right: 10),
                    fillColor: TextFieldBackgroundColor,
                  ),
                  controller: messageController,
                ),
              ),
            ),
            SizedBox(width: 10),
            MaterialButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5))),
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 7),
              elevation: 0,
              minWidth: 0,
              height: 0,
              color: SendButtonColor,
              child: Text(
                "send",
                style:
                    TextStyle(color: Colors.white, fontSize: RegularFontSize),
              ),
              onPressed: initSendMessage,
            )
          ],
        ),
      ),
    );
  }

  //Listens to RPC API triggers
  //calls setState()  to refresh chat body
  _initChatSubscription() async {
    _chatSubscription = widget.firebaseAPIHandler
        .getCollectionStream()
        .stream
        .listen((snapshot) {
      setState(() {});
    }, onError: (error) {
      setState(() {
        isSubscriptionActive = false;
      });
    });
  }

  //Initialize data to be posted
  Future<void> initSendMessage() async {
    if (!isSubscriptionActive) {
      _initChatSubscription();
    }
    //isUsingIntlTime == true is set in Global data
    if (isUsingIntlTime) {
      if (messageController.text.length > 0) {
        //Gets timezone-specifix datetime so that users in different timezones can use the app properly
        await widget.firebaseAPIHandler.getServerTime().then(
            (serverTime) async {
          String now = DateTime.parse(serverTime).toIso8601String();
          _postRequest(now);
        }, onError: (e) {
          widget.commonWidgets.showAlert(context, "Error",
              "Could not retrieve Server time. Please try again.");
        });
      }
    } else {
      //Uses local DateTime
      String now = DateTime.now().toIso8601String() + "Z";
      _postRequest(now);
    }
  }

  //Posts message to FireStore db
  _postRequest(String now) async {
    if (messageController.text.isNotEmpty) {
      await widget.firebaseAPIHandler.postMessage(_postToMap(now)).then(
          (result) {
        print(result.message);
        if (result.runtimeType == APIResult) {
          if (result.message != REQUEST_SUCCESS) {
            widget.commonWidgets.showAlert(context, ERROR, QUOTA_ERROR);
          }
        } else {
          widget.commonWidgets.showAlert(context, ERROR, result.toString());
          isSubscriptionActive = false;
        }
        _clearTextBoxAndScroll();
      }, onError: (error) {
        Navigator.pop(context);
        widget.commonWidgets.showAlert(context, ERROR, GeneralError);
      });
    }
  }

  String _postToMap(String now) {
    var map = <String, Map<String, Map<String, String>>>{
      'fields': <String, Map<String, String>>{
        'message': new HashMap.from({'stringValue': messageController.text}),
        'username': new HashMap.from({'stringValue': widget.email}),
        'createDate': new HashMap.from({'timestampValue': now}),
      },
    };
    return jsonEncode(map);
  }

  _clearTextBoxAndScroll() {
    _scrollToBottom();
    messageController.clear();
  }

  //For dynamically expanding message textfield for messages with multiple lines
  void _checkInputHeight() async {
    int count = messageController.text.split('\n').length;

    if (count == 0 && _inputHeight == 60.0) {
      return;
    }
    if (count <= 5) {
      var newHeight = count == 0 ? 50.0 : 28.0 + (count * 18.0);
      setState(() {
        _inputHeight = newHeight;
      });
    }
  }
}
