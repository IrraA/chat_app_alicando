import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../data/global.dart';

//Default button style for the app

class RoundedButton extends StatelessWidget {
  RoundedButton({this.title, this.color, @required this.onPressed});

  final Color color;
  final String title;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      height: 50,
      padding: EdgeInsets.symmetric(horizontal: 18.0),
      margin: EdgeInsets.only(bottom: 10),
      child: FlatButton(
        color: color,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        onPressed: onPressed,
        child: Text(
          title,
          style: TextStyle(
            fontSize: HeavyFontSize,
            fontWeight: HeavyFontWeight,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
