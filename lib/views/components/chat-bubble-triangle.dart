import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../data/global.dart';

//Custom painted chat bubble triangles for ChatBubble component

class ChatBubbleTriangle extends CustomPainter {
  final bool me;
  ChatBubbleTriangle(this.me);
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()..color = PrimaryColor;

    var path = Path();
    if (me) {
      path.lineTo(0, 5);
      path.lineTo(0, 5);
      path.lineTo(5, 0);
      path.lineTo(0, -5);
    } else {
      path.lineTo(0, -5);
      path.lineTo(0, -5);
      path.lineTo(-5, 0);
      path.lineTo(0, 5);
    }

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
