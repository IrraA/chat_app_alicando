import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../data/global.dart';
import '../signup-login-view.dart';

//Collection of reusable widgets

class CommonWidgetsCollection {
  static CommonWidgetsCollection _instance;
  factory CommonWidgetsCollection() =>
      _instance ??= new CommonWidgetsCollection._();
  CommonWidgetsCollection._();

  void openLoadingDialog(BuildContext context) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            elevation: 0,
            backgroundColor: Colors.transparent,
            content: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  width: 40,
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(PrimaryColor),
                  ),
                )
              ],
            ));
      },
    );
  }

  Future<void> showAlert(context, String title, String message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Wrap(
            children: <Widget>[
              ListBody(
                children: <Widget>[
                  Text(message),
                ],
              ),
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('OK',
                  style: TextStyle(
                      color: PrimaryColor, fontSize: RegularFontSize)),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget header(bool isLogoutButtonShown, BuildContext context) {
    return Container(
      width: double.maxFinite,
      margin: EdgeInsets.fromLTRB(18, 10, 18, 0),
      decoration: BoxDecoration(
          border: Border(
              bottom:
                  BorderSide(color: TextColor.withOpacity(0.3), width: 1.5))),
      child: Stack(
        children: <Widget>[
          Positioned(
            child: Align(
              alignment: FractionalOffset.center,
              child: Padding(
                padding: EdgeInsets.only(
                    top: 14, bottom: isLogoutButtonShown ? 0 : 15),
                child: Text(
                  "Chat app",
                  style: TextStyle(
                      fontSize: HeavyFontSize,
                      fontWeight: HeavyFontWeight,
                      color: HeaderTextColor),
                ),
              ),
            ),
          ),
          isLogoutButtonShown
              ? Positioned(
                  child: Align(
                    alignment: FractionalOffset.centerRight,
                    child: MaterialButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(5))),
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 7),
                      elevation: 0,
                      minWidth: 0,
                      height: 0,
                      color: SendButtonColor,
                      child: Text(
                        "Log out",
                        style: TextStyle(
                            color: Colors.white, fontSize: RegularFontSize),
                      ),
                      onPressed: () {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SignUpLoginView(
                                      isLogin: false,
                                    )));
                      },
                    ),
                  ),
                )
              : SizedBox()
        ],
      ),
    );
  }
}
