import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../data/global.dart';
import 'chat-bubble-triangle.dart';

//Chat Bubble component used in ChatView

class ChatBubble extends StatelessWidget {
  final String from;
  final String text;

  final bool me;

  const ChatBubble({Key key, this.from, this.text, this.me}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin:
          EdgeInsets.only(right: me ? 5 : 35, left: me ? 35 : 5, bottom: 15),
      child: Column(
        crossAxisAlignment:
            me ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Stack(
            alignment:
                me ? FractionalOffset.bottomRight : FractionalOffset.bottomLeft,
            children: <Widget>[
              Material(
                color: PrimaryColor,
                borderRadius: BorderRadius.circular(5.0),
                child: Container(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    text,
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: RegularFontWeight,
                        fontSize: RegularFontSize),
                  ),
                ),
              ),
              Positioned(
                  bottom: 15,
                  child: CustomPaint(painter: ChatBubbleTriangle(me)))
            ],
          ),
          Text(from,
              style:
                  TextStyle(color: TextColor, fontWeight: RegularFontWeight)),
        ],
      ),
    );
  }
}
