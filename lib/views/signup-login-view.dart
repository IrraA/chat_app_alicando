import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '../data/global.dart';
import '../data/models/api-result.dart';
import '../services/api/firebase-api-handler.dart';
import 'chat-view.dart';
import 'components/common-widgets.dart';
import 'components/rounded-button.dart';

// Set Sign up and Login page up in one view so that widgets can be reused
class SignUpLoginView extends StatefulWidget {
  final bool isLogin;
  SignUpLoginView({@required this.isLogin});

  //Init imported classes
  final FirebaseAPIHandler firebaseAPIHandler = new FirebaseAPIHandler();
  final CommonWidgetsCollection commonWidgets = new CommonWidgetsCollection();
  @override
  State<StatefulWidget> createState() {
    return SignUpLoginViewState();
  }
}

class SignUpLoginViewState extends State<SignUpLoginView> {
  TextEditingController _userNameController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
  bool _userNameHasError = false;
  bool _passwordHasError = false;
  String _buttonTitle;
  String _linkTitle;
  bool _isLogin;

  @override
  void initState() {
    _setIsLogin(widget.isLogin);
    super.initState();
  }

  @override
  void dispose() {
    _userNameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: SingleChildScrollView(
            child: SizedBox(
              width: double.maxFinite,
              height: deviceSize.height,
              child: Column(
                children: <Widget>[
                  widget.commonWidgets.header(false, context),
                  _userNameTextField(),
                  _passwordTextField(),
                  _button(),
                  _link(),
                  _termsAndServices()
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _userNameTextField() {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.only(bottom: 20, top: 25),
      padding: EdgeInsets.symmetric(horizontal: 28),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextField(
              controller: _userNameController,
              decoration: _defaultTextFieldDecoration("User name")),
          _userNameHasError ? _errorMessage() : SizedBox()
        ],
      ),
    );
  }

  _passwordTextField() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 28),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextField(
              controller: _passwordController,
              obscureText: true,
              decoration: _defaultTextFieldDecoration("password")),
          _passwordHasError ? _errorMessage() : SizedBox()
        ],
      ),
    );
  }

  _button() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      margin: EdgeInsets.only(top: 20),
      child: RoundedButton(
        onPressed: () {
          _validateFields();
          if (!_userNameHasError && !_passwordHasError) {
            _isLogin ? loginUser() : signUp();
          }
        },
        color: PrimaryColor,
        title: _buttonTitle,
      ),
    );
  }

  _link() {
    return Container(
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: TextColor, width: 1.5))),
      child: RichText(
        text: TextSpan(
          style: TextStyle(
              fontSize: RegularFontSize - 2.2,
              fontWeight: RegularFontWeight,
              color: TextColor),
          text: _linkTitle,
          recognizer: new TapGestureRecognizer()
            ..onTap = () async {
              _setIsLogin(!_isLogin);
            },
        ),
      ),
    );
  }

  _termsAndServices() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 28, vertical: 20),
      child: Text(
        TermsAndServicesText,
        style: TextStyle(
          wordSpacing: 2.5,
          color: TextColor,
          fontSize: RegularFontSize,
          fontWeight: RegularFontWeight,
        ),
      ),
    );
  }

  _errorMessage() {
    return Padding(
      padding: EdgeInsets.only(top: 5, left: 13),
      child: Text(
        ErrorMessage,
        style: TextStyle(
          color: ErrorTextColor,
          fontSize: HeavyFontSize,
          fontWeight: RegularFontWeight,
        ),
      ),
    );
  }

  _defaultTextFieldDecoration(String text) {
    return InputDecoration(
      hintText: text,
      hintStyle: TextStyle(
          fontSize: HeavyFontSize,
          fontWeight: RegularFontWeight,
          color: TextColor),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(5),
        borderSide: BorderSide(width: 0, style: BorderStyle.none),
      ),
      filled: true,
      contentPadding: EdgeInsets.all(13),
      fillColor: TextFieldBackgroundColor,
    );
  }

  //For setting Sign up and Login page
  _setIsLogin(bool isLogin) {
    setState(() {
      _isLogin = isLogin;
      _buttonTitle = _isLogin ? Login : SignUp;
      _linkTitle = _isLogin ? SignUp : Login;
      _userNameController.text = "";
      _passwordController.text = "";
      _userNameHasError = false;
      _passwordHasError = false;
    });
  }

  _validateFields() {
    String username = _userNameController.text;
    String password = _passwordController.text;
    setState(() {
      _userNameHasError = ((username.isEmpty) ||
          (username.length < MinTextLength) ||
          (username.length > MaxTextLength));
      _passwordHasError = ((password.isEmpty) ||
          (password.length < MinTextLength) ||
          (password.length > MaxTextLength));
    });
  }

  _initChat() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) =>
                ChatView(_userNameController.text, DateTime.now())));
  }

  Future<void> signUp() async {
    widget.commonWidgets.openLoadingDialog(context);
    widget.firebaseAPIHandler
        .signUpUser(_userNameController.text, _passwordController.text)
        .then((result) {
      Navigator.pop(context);
      if (result.runtimeType == APIResult) {
        APIResult response = result;
        _buttonCallback(response.message);
      } else {
        widget.commonWidgets.showAlert(context, ERROR, result.toString());
      }
    }, onError: (error) {
      Navigator.pop(context);
      widget.commonWidgets.showAlert(context, ERROR, GeneralError);
    });
  }

  Future<void> loginUser() async {
    widget.commonWidgets.openLoadingDialog(context);
    widget.firebaseAPIHandler
        .loginUser(_userNameController.text, _passwordController.text)
        .then((result) {
      Navigator.pop(context);
      if (result.runtimeType == APIResult) {
        APIResult response = result;
        _buttonCallback(response.message);
      } else {
        widget.commonWidgets.showAlert(context, ERROR, result.toString());
      }
    }, onError: (error) {
      Navigator.pop(context);
      widget.commonWidgets.showAlert(context, ERROR, GeneralError);
    });
  }

  _buttonCallback(String message) {
    switch (message) {
      case LOGIN_SUCCESS:
        _initChat();
        break;
      case REQUEST_SUCCESS:
        _initChat();
        break;
      case INVALID_EMAIL:
        _setAllFieldsError();
        break;
      case ALREADY_EXISTS:
        _setAllFieldsError();
        break;
      case INVALID_PASSWORD:
        setState(() {
          _passwordHasError = true;
        });
        break;
      default:
        _setAllFieldsError();
        break;
    }
  }

  _setAllFieldsError() {
    setState(() {
      _userNameHasError = true;
      _passwordHasError = true;
    });
  }
}
