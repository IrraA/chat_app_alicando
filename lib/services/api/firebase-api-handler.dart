import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'package:firedart/firedart.dart';
import 'package:firedart/firestore/firestore.dart';
import 'package:http/http.dart';

import '../../data/global.dart';
import '../../data/models/api-result.dart';
import '../utilities/firestore-doc-response.dart';
import '../utilities/http-exception.dart';

//All firestore requests are done using REST API with a public firestore cloud db
//
//This is where all Firestore transactions are made
//
//Only firestore transaction used without using REST API is getCollectionStream(),
//since streaming messages from firestore is required for this app and it can only
//be done by using RPC API implemented by Firedart library

class FirebaseAPIHandler {
  static FirebaseAPIHandler _instance;
  factory FirebaseAPIHandler() => _instance ??= new FirebaseAPIHandler._();
  FirebaseAPIHandler._();

  //Sign up user
  //Response is mapped using custom http response mapper FireStoreDocResponse
  //Please note that the password is not encrypted, make sure that you are not using real usernames and passwords
  Future signUpUser(String username, String password) async {
    try {
      APIResult result = new APIResult();
      final response = await post(
          (FirestoreBaseUrl +
              FirestoreDatabase +
              "documents/users?documentId=" +
              username),
          body: _userToMap(username, password));
      result = FireStoreDocResponse().response(response);
      return result;
    } on SocketException {
      return FetchDataException(NoInternetConnection);
    } on Exception {
      return InvalidInputException(ServerError);
    }
  }

  //Login user
  //Since Firestore REST API is used, the query used to validate user is done by structuredQuery
  //The flow: check if username exists in db -> if exists, get data and compare password -> assign message to result model
  Future loginUser(String username, String password) async {
    try {
      APIResult result = new APIResult(message: INVALID_EMAIL);
      var body = jsonEncode({
        "structuredQuery": {
          //select from users
          "from": {
            "collectionId": "users",
          },
          //where field user is equals to givem username
          "where": {
            "fieldFilter": {
              "field": {"fieldPath": "username"},
              "op": "EQUAL",
              "value": {"stringValue": username}
            },
          },
        },
      });

      final response = await post(
          (FirestoreBaseUrl + FirestoreDatabase + "documents:runQuery"),
          headers: Headers,
          body: body);

      //Had to traverse the response body this way because auto-mapping limitations
      var documentDecoded = jsonDecode(response.body)[0]["document"];
      if (documentDecoded != null) {
        String dbPassword =
            documentDecoded["fields"]["password"]["stringValue"];
        if (dbPassword == password) {
          result.message = LOGIN_SUCCESS;
        } else {
          result.message = INVALID_PASSWORD;
        }
      }
      return result;
    } on SocketException {
      return FetchDataException(NoInternetConnection);
    } on Exception {
      return InvalidInputException(ServerError);
    }
  }

  //Post message to Firestore db using REST API
  Future postMessage(String data) async {
    try {
      APIResult result = new APIResult();
      final response = await post(
          (FirestoreBaseUrl +
              FirestoreDatabase +
              "documents/" +
              ChatCollectionName),
          body: data);
      result = FireStoreDocResponse().response(response);
      return result;
    } on SocketException {
      return FetchDataException(NoInternetConnection);
    } on Exception {
      return InvalidInputException(ServerError);
    }
  }

  //uses Firedart library to return stream of given collection name
  CollectionReference getCollectionStream() {
    return Firestore.instance.collection(ChatCollectionName);
  }

  //Maps and encodes given username and password for FireStore REST API to read and process
  dynamic _userToMap(String username, String password) {
    var map = <String, Map<String, Map<String, String>>>{
      'fields': <String, Map<String, String>>{
        'username': new HashMap.from({'stringValue': username}),
        'password': new HashMap.from({'stringValue': password}),
      },
    };
    return jsonEncode(map);
  }

  //Used if Global data isUsingIntlTime is set to "true"
  //Since there is no way to get SERVER_REQUEST_TIME from Firestore before posting message without using Firestore Cloud Plugin, this method is used to get datetime from a specific timezone (Europr/London)
  //However it takes time to get datetime from this and sending messages becomes slower
  Future<String> getServerTime() async {
    final response = await get(
      WorldTimeURL,
    );
    return jsonDecode(response.body)["datetime"];
  }
}
