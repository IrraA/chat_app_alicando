//Used to help handle HTTP request errors

class HTTPException implements Exception {
  final _message;
  final _prefix;

  HTTPException([this._message, this._prefix]);

  String toString() {
    return "$_prefix$_message";
  }
}

class FetchDataException extends HTTPException {
  FetchDataException([String message])
      : super(message, "Error During Communication: ");
}

class BadRequestException extends HTTPException {
  BadRequestException([message]) : super(message, "Invalid Request: ");
}

class UnauthorisedException extends HTTPException {
  UnauthorisedException([message]) : super(message, "Unauthorised: ");
}

class InvalidInputException extends HTTPException {
  InvalidInputException([String message]) : super(message, "Invalid Input: ");
}
