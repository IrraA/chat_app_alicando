import 'package:http/http.dart' as http;
import 'dart:convert';

import '../../data/global.dart';
import '../../data/models/api-result.dart';

//Used for easier decoding of response data from FireStore

class FireStoreDocResponse {
  static FireStoreDocResponse _instance;
  factory FireStoreDocResponse() => _instance ??= new FireStoreDocResponse._();

  FireStoreDocResponse._();

  dynamic response(http.Response response) {
    var decoded = json.decode(response.body.toString());
    return APIResult(
        message: decoded["error"] == null
            ? decoded["fields"] == null ? null : REQUEST_SUCCESS
            : jsonDecode(jsonEncode(decoded["error"]))["status"],
        result: decoded["fields"] == null ? decoded : decoded["fields"]);
  }
}
