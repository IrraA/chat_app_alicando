import "package:flutter/cupertino.dart";
import "package:flutter/material.dart";

//Global data
//Values assigned here are reusable values or values for configuration

//Colors values
const Color PrimaryColor =  Color.fromRGBO(134, 224, 10, 1.0);
const Color SecondaryColor =  Color.fromRGBO(98, 163, 4, 1.0);
const Color AppBackgroundColor =  Color.fromRGBO(255, 255, 255, 1.0);
const Color SendButtonColor =  Color.fromRGBO(102, 102, 102, 1.0);
const Color TextColor =  Color.fromRGBO(102, 120, 136, 1.0);
const Color HeaderTextColor =  Color.fromRGBO(53, 53, 53, 1.0);
const Color ErrorTextColor =  Color.fromRGBO(205, 57, 57, 1.0);
const Color TextFieldBackgroundColor =  Color.fromRGBO(228, 237, 240, 0.5);
//Textstyle values
const FontWeight RegularFontWeight = FontWeight.w500;
const FontWeight HeavyFontWeight = FontWeight.w800;
const double RegularFontSize = 16.0;
const double HeavyFontSize = 20.0;
const double SmallFontSize = 14.0;
//Strings
const String TermsAndServicesText = "By signing up, you agree to the Terms of Service and Policy Privacy, including Cookie Use. Others will be able to find you by searching for your email address or phone number when provided.";
const String ErrorMessage = "Value is incorrect";
const String UserNameSuffix = "@chatapp.com";
const String Login = "Login";
const String SignUp =  "Sign up";
//FireStore values
const Map<String,String> Headers = {"Content-Type":"application/json"};
const String FirestoreBaseUrl = "https://firestore.googleapis.com/v1beta1/";
const String FirestoreDatabase = "projects/ligph-chat-app-exam-alicando/databases/(default)/";
const String FireBaseApiKey = "AIzaSyB7I5-viYUTO92oDPyAJ9bZt3_1Qrc-l1E";
const String FireBaseProjectId = "ligph-chat-app-exam-alicando";
const String ChatCollectionName = "Chatroom-3";
//URL used to get timezone-specific Datetime
const String WorldTimeURL = "http://worldtimeapi.org/api/timezone/Europe/London";
//Messages
const String NoInternetConnection = "No Internet connection";
const String GeneralError = "Something went wrong. Please try again.";
const String ServerError = "Server Error";
const String ERROR = "Error";
const String REQUEST_SUCCESS = "REQUEST_SUCCESS";
const String LOGIN_SUCCESS = "LOGIN_SUCCESS";
const String INVALID_EMAIL = "INVALID_EMAIL";
const String INVALID_PASSWORD = "INVALID_PASSWORD";
const String ALREADY_EXISTS = "ALREADY_EXISTS";
const String QUOTA_ERROR = "Request Failed. Please check current Firestore read qouta as it may have been used up.";
//Username and Password max and min length settings
const int MinTextLength = 8;
const int MaxTextLength = 16;
//Set to true if using server time is required
const bool isUsingIntlTime = true;


